import time
from matplotlib import pyplot




def permutar_tudo(lista_func_permuta): # Função recursiva de permutação de caminhos possíveis, travando um ponto como primeiro
  lst_permuta_compl = []                        # e permutando o resto até que o primeiro ponto na primeira iteração seja o ultimo na permutação gerada.
  if len(lista_func_permuta) == 0 or len(lista_func_permuta) == 1: # casos base
    return [lista_func_permuta]
  for i in range(len(lista_func_permuta)):
    pos_travada = lista_func_permuta[i]
    resto_permt = lista_func_permuta[:i] + lista_func_permuta[i + 1:]
    for elemnt in permutar_tudo(resto_permt):
      lst_permuta_compl.append([pos_travada] + elemnt)
  return lst_permuta_compl



lista_entrada = []

m, n = input().split() # entrada das dimensões da matriz
i = 0
m = int(m)

for i in range(m): # entrada dos dados da matriz
  #print(i)
  lista_entrada.append(input().split())


lista_posicao = []

col = 0
lin = 0

for lista in lista_entrada: # busca pelos pontos da matriz que são diferentes de 0, pois estes representam pontos
  col = 0                   # armazena ponto e coordenadas com ajuda dos contadores lin col
  for elemento in lista:
    if elemento != '0':
      lista_posicao.append([elemento, (col, lin)])
    col += 1
  lin += 1

permutacao_total = permutar_tudo(lista_posicao) # chama a função de permutação e passa a lista com pontos e coordenadas
caminho = []
distancia = 10000 # recebe um valor alto inicialmente para que a primeira distancia calculada seja considerada menor
                  # e ser armazenada até ser comparada com outra distancia

for instancia in permutacao_total: # cálculo das distancias entre pontos
  indice = 0
  distancia_tmp = 0
  for letra, coord in instancia:
    indice += 1
    if indice == len(instancia):
      break
    distancia_tmp += abs(instancia[indice][1][0] - coord[0]) + abs(instancia[indice][1][1] - coord[1])
  indice -= 1
  distancia_tmp += abs(instancia[0][1][0] - instancia[indice][1][0]) + abs(instancia[0][1][1] - instancia[indice][1][1])

  if distancia > distancia_tmp:
    caminho = instancia
    distancia = distancia_tmp

print(caminho)

mapax = []
mapay = []


rota = ""

for letra, pos in caminho: # obtenção da resposta no formato desejado e plotagem de grafico
  rota+= letra
  mapax.append(pos[0])
  mapay.append(pos[1])
  pyplot.annotate(letra,(pos[0],pos[1]))

mapax.append(mapax[0])
mapay.append(mapay[0])

pyplot.title("Caminho percorrido pelo drone durante rota de entrega")
pyplot.xlabel("Coordenada X dos pontos de entrega")
pyplot.ylabel("Coordenada Y dos pontos de entrega")
pyplot.plot(mapax,mapay, linestyle='-.', marker='*', color='g', markersize=8)
pyplot.xticks(fontsize=10)
pyplot.yticks(fontsize=10)

fim = time.process_time()
print(rota)
print(fim)
pyplot.show()